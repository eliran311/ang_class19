import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  user: Observable<User| null>
 
  constructor(public afAuth:AngularFireAuth,private router:Router) { 
   this.user = this.afAuth.authState;
}

signup(email:string, password:string){
     this.afAuth
     .auth.createUserWithEmailAndPassword(email,password)
     .then
     (user =>  {
       this.router.navigate(['/books']);
    });
  }
    logout(){
      this.afAuth.auth.signOut().then(res => console.log('successful logout'));
     }
     login(email:string, password:string){
      this.afAuth
      .auth.signInWithEmailAndPassword(email,password)
      .then
      (user =>{
        this.router.navigate(['/books']);
     })
     }
}
