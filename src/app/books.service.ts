import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { collectExternalReferences } from '@angular/compiler';

 

@Injectable({
  providedIn: 'root'
})
export class BooksService {
 
 // books: any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]

/*  addBooks(){
    setInterval(() => 
      this.books.push({title:'A new one', author:'New author'})
  , 5000);    
  }
/*
  getBooks(): any {
    const booksObservable = new Observable(observer => {
           setInterval(() => 
               observer.next(this.books)
           , 5000);
    });

    return booksObservable;
}
*/
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  booksCollection:AngularFirestoreCollection;

  getBooks(userId:string):Observable<any[]>{
    // return this.db.collection('books').valueChanges({idField:'id'});
  this.booksCollection = this.db.collection(`users/${userId}/books`)
  return this.booksCollection.snapshotChanges().pipe(
    map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data; 
        }


      )
    )
  )
  }
  
  addBook(userId:string,  title:string,author:string){
    const book = {title:title, author:author};
  // this.db.collection('books').add(book)
  this.userCollection.doc(userId).collection('books').add(book);

}
deletebooks(id:string,userId:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}
updateBook(userId:string, id:string,title:string,author:string){
this.db.doc(`users/${userId}/books/${id}`).update({
  title:title,author:author});
}
getBook(id:string, userId:string):Observable<any>{
  return this.db.doc(`users/${userId}/books/${id}`).get();
}
  constructor(private db:AngularFirestore) { }
}