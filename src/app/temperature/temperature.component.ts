import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { WeatherService } from './../temp.service';
import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperaturesComponent implements OnInit {

   
  constructor(private route: ActivatedRoute, 
              private weatherservice:WeatherService) { }

  likes = 0;
  temperature;
  image;
  errorMessage:String;
  hasError:Boolean=false; 
  city;
  tempData$:Observable<Weather>

  addLikes(){
    this.likes++
  }

  ngOnInit() {
    //this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.weatherservice.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error=>{
        this.hasError= true;
        this.errorMessage = error.message;
        console.log('in the component' + error.nessage);
      } 
    )
  }

}
