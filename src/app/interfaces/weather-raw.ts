export interface WeatherRaw {
  
  weather: [
    {
     description: String,
      icon: String,
    }
];
  
  main: {
    temp: number,
  };
  sys: {
    country: "GB",
  };
coord:{
    lon:number,
    lat:number
};
  name: String
}

