
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import {  ImagesService } from '../images.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading..";
  categoryImage:string;
  constructor(public classifyService:ClassifyService,
    public imagesService:ImagesService ) { }

  ngOnInit() {
  
  this.classifyService.classify().subscribe(
    res => {
      this.category = this.classifyService.categories[res];
      console.log(this.category);
      this.categoryImage = this.imagesService.images[res];
    } 
  )
  
  }

}
