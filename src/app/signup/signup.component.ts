import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
 



  constructor(private signupservice:AuthService,private router: Router,private route:ActivatedRoute) { }
email:string;
password:string;
  ngOnInit() {

  }
  onSubmit(){
    this.signupservice.signup(this.email,this.password);
    this.router.navigate(['/books']);
  }

}
