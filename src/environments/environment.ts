// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyB7RIZU2DPnENPQrEb1Ru0kAqP94P0xCtw",
    authDomain: "classang-f2b2d.firebaseapp.com",
    databaseURL: "https://classang-f2b2d.firebaseio.com",
    projectId: "classang-f2b2d",
    storageBucket: "classang-f2b2d.appspot.com",
    messagingSenderId: "532850586014",
    appId: "1:532850586014:web:3ef243eba1c6df3646268f"
   }
  };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
